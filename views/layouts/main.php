<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use app\widgets\Menulomb;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
// use app\models\Settings;

// $this->registerJs('
// ');

AppAsset::register($this);
$thisPage = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html  lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<title>Главная страница</title>
	<meta content="Chernyh Mihail" name="author">
	<meta content="Ломбардия" name="description">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="HandheldFriendly" content="true">
	<meta name="format-detection" content="telephone=no">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<?php $this->registerCsrfMetaTags() ?>
	<link rel="shortcut icon" href="/assets/images/favicon.png" type="image/x-icon">
	<?php $this->head() ?>
</head>

<body class="page-<?= ($thisPage == 'index')?'home':$thisPage;?>">
<?php $this->beginBody() ?>
	<div class="page-wrapper">
		<header class="page-header">
			<div class="container-fluid">
				<div class="page-header__inner">
					<div class="page-header__logo">
						<div class="logo"><a class="logo__link" href="/"><img class="img-fluid logo__full" src="/assets/img/logo.png" alt=""><img class="img-fluid logo__small" src="/assets/img/logo-icon.svg" alt=""></a></div>
					</div>
					<div class="page-header__menu">
<?php echo Menulomb::widget(); ?>
					</div>
					<div class="page-header__lang">
						<div class="lang dropdown">
							<div class="dropdown-toggle" role="button" id="lang-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<div class="lang-current"><img class="lang-current__icon" src="/assets/img/icons/russia.svg" alt=""><span class="lang-current__text">RUS</span></div>
							</div>
							<div class="dropdown-menu" aria-labelledby="lang-dropdown"><a class="dropdown-item" href="#">
									<div class="lang-item"><img class="lang-item__icon" src="/assets/img/icons/russia.svg" alt="RU"><span class="lang-item__text">RUS</span></div>
								</a>
								<div class="dropdown-divider"></div><a class="dropdown-item" href="#">
									<div class="lang-item"><img class="lang-item__icon" src="/assets/img/icons/united-states.svg" alt="EN"><span class="lang-item__text">ENG</span></div>
								</a>
							</div>
						</div>
					</div>
					<div class="page-header__phone"><a class="phone-link" href="tel:74951490909">+7(495) 149-09-09</a></div>
					<div class="page-header__search">
						<div class="btn-search"><img src="/assets/img/icons/search.svg" alt=""></div>
					</div>
					<div class="page-header__user"><a class="link-user" href="/personal-area.html"><img src="/assets/img/icons/user.svg" alt=""></a></div>
					<div class="page-header__mobile-menu"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobileMenu" aria-controls="mobileMenu" aria-expanded="false" aria-label="Toggle navigation"><span class="nav-icon"><span></span><span></span><span></span></span></button></div>
				</div>
				<div class="page-header__mobile-menu-wrap">
					<div class="collapse navbar-collapse" id="mobileMenu">
						<ul class="user-menu">
							<li class="user-menu-item"><a class="user-menu-link" href="/site/user-info"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-1.svg" alt="sidebar-icon"></span><span class="user-menu-text">Личные данные</span></a></li>
							<li class="user-menu-item"><a class="user-menu-link" href="/site/my-loans"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-2.svg" alt="sidebar-icon"></span><span class="user-menu-text">Мои займы</span></a></li>
							<li class="user-menu-item"><a class="user-menu-link" href="/site/loan-history"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-3.svg" alt="sidebar-icon"></span><span class="user-menu-text">История займов</span></a></li>
							<li class="user-menu-item"><a class="user-menu-link" href="/site/promotions"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-4.svg" alt="sidebar-icon"></span><span class="user-menu-text">Промокоды</span></a></li>
							<li class="user-menu-item"><a class="user-menu-link" href="/site/exit"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-5.svg" alt="sidebar-icon"></span><span class="user-menu-text">Выйти</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<main class="page-main">
			<div class="container-xl">
				<div class="row">
						<?php if ($thisPage == 'index') { ?>
					<div class="col-lg-3"></div>
						<?php } else { ?>
					<div class="col-lg-3 d-none d-lg-block d-md-none">
						<aside class="sidebar">
							<ul class="user-menu">
								<li class="user-menu-item"><a class="user-menu-link<?=($thisPage == 'user-info')?' user-menu-link--active':''; ?> " href="/site/user-info"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-1.svg" alt="sidebar-icon"></span><span class="user-menu-text">Личные данные</span></a></li>
								<li class="user-menu-item"><a class="user-menu-link<?=($thisPage == 'my-loans')?' user-menu-link--active':''; ?>" href="/site/my-loans"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-2.svg" alt="sidebar-icon"></span><span class="user-menu-text">Мои займы</span></a></li>
								<li class="user-menu-item"><a class="user-menu-link<?=($thisPage == 'loan-history')?' user-menu-link--active':''; ?>" href="/site/loan-history"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-3.svg" alt="sidebar-icon"></span><span class="user-menu-text">История займов</span></a></li>
								<li class="user-menu-item"><a class="user-menu-link<?=($thisPage == 'promotions')?' user-menu-link--active':''; ?>" href="/site/promotions"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-4.svg" alt="sidebar-icon"></span><span class="user-menu-text">Промокоды</span></a></li>
								<li class="user-menu-item"><a class="user-menu-link" href="/site/exit"><span class="user-menu-icon"><img src="/assets/img/icons/sidebar-5.svg" alt="sidebar-icon"></span><span class="user-menu-text">Выйти</span></a></li>
							</ul>
						</aside>
					</div>
						<?php } ?>
				<?php echo $content; ?>
				</div>
			</div>
		</main>
		<footer class="page-footer">
			<div class="container-fluid">
				<div class="page-footer__top-line">
					<div class="page-footer__menu">
<?php echo Menulomb::widget(); ?>
					</div>
				</div>
				<div class="page-footer__bottom-line">
					<div class="page-footer__mobile-app">
						<div class="mobile-app">
							<div class="mobile-app__title"><span>Мобильное<br> приложение</span></div>
							<div class="mobile-app__link"><a href="#!"><img src="/assets/img/icons/ios-app.png" alt="ios-app"></a></div>
						</div>
					</div>
					<div class="page-footer__social">
						<div class="social">
							<div class="social__title"> <span>Напишите нам<br> в мессенджер</span></div>
							<div class="social__list">
								<ul class="social-list">
									<li class="social-item"><a class="social-link" href="#!"><img src="/assets/img/icons/instagram.svg" alt="instagram"></a></li>
									<li class="social-item"><a class="social-link" href="#!"><img src="/assets/img/icons/facebook.svg" alt="facebook"></a></li>
									<li class="social-item"><a class="social-link" href="#!"><img src="/assets/img/icons/youtube.svg" alt="youtube"></a></li>
									<li class="social-item"><a class="social-link" href="#!"><img src="/assets/img/icons/wathsapp.svg" alt="wathsapp"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="fixed-menu">
				<ul class="fixed-menu-list">
					<li class="fixed-menu-item"><a class="fixed-menu-link" href="/"><span class="fixed-menu-icon"><img src="/assets/img/icons/menu-home.svg" alt="Главная"></span><span class="fixed-menu-text">Главная</span></a></li>
					<li class="fixed-menu-item"><a class="fixed-menu-link" href="#!"><span class="fixed-menu-icon"><img src="/assets/img/icons/menu-mail.svg" alt="Напишите нам"></span><span class="fixed-menu-text">Напишите нам</span></a></li>
					<li class="fixed-menu-item"><a class="fixed-menu-link" href="#!"><span class="fixed-menu-icon"><img src="/assets/img/icons/menu-phone.svg" alt="Позвоните нам"></span><span class="fixed-menu-text">Позвоните нам</span></a></li>
					<li class="fixed-menu-item"><a class="fixed-menu-link" href="#!"><span class="fixed-menu-icon"><img src="/assets/img/icons/menu-whatsapp.svg" alt="WhatsApp"></span><span class="fixed-menu-text">WhatsApp</span></a></li>
					<li class="fixed-menu-item"><a class="fixed-menu-link" href="/site/user-info"><span class="fixed-menu-icon"><img src="/assets/img/icons/menu-user.svg" alt="Профиль"></span><span class="fixed-menu-text">Профиль</span></a></li>
				</ul>
			</div>
		</footer><!-- Modal-->
		<div class="modal fade" id="order-redemption" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-dialog-centered">
				<div class="modal-content"><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<div class="modal-body" id="modalreq">
					<?= Html::beginForm(['#'], 'get', ['id' => 'formkeeper']); ?>
					<?= Html::input('hidden', 'inpids', '', array('id' => 'inpids')) ?>
					<?= Html::input('hidden', 'inpsumm', '', array('id' => 'inpsumm')) ?>
						<h3>Оплата</h3>
						<p>Для оплаты задолженности банковской картой<br> Вы будете переведены к нашему партнеру.</p><a class="btn btn-block btn-danger" id="btnpaykeeper" href="#" onclick="$('#order-redemption').modal('hide');window.open('/ajax/paykeeper?inpids='+ $('#inpids').val()+'&inpsumm=' + $('#inpsumm').val()); return false;//topaykeeper()" data-ids="">Перейти к оплате <span id="summProcModal">341</span> р</a>
					<?= Html::endForm() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>

</html>