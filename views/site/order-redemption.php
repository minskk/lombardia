<?php
use yii\helpers\Url;
use app\models\Settings;

$request = Yii::$app->request;
$ids = $request->get('ids', '');

$this->title = 'Текущие займы по отделению';
$this->registerJs('
	$(".js-date-go").datepicker({
		position: "bottom right",
		autoClose: true,
		onSelect: function(formattedDate, date, inst) {
			document.location.href="/site/order-redemption?date=" + formattedDate + "' . $ids . '";
		}
	});
	function checkSummProc(officeid) {
		var summ = 0,
		zalog = 0,
		zalogAll = 0,
		all;
		var ids = [];
		$("#office_"+ officeid +" input:checkbox:checked").each(function(){
			summ = summ+parseInt($(this).val());
			ids.push( $(this).attr("data-ids") );
			zalog += parseInt($(this).parent().parent().parent().parent().attr("data-price"));
		});
		$(".loan-item-small").each(function(){
			zalogAll += parseInt($(this).attr("data-price"));
		});
		
		if (summ == 0) {
			summ = parseInt($("#office_"+ officeid +" #summProc").attr("data-val"));
			all = (summ + zalogAll);
			$("#office_"+ officeid +" #countProc").text("все");
			$("#summZaim").text(zalogAll + " P");
		} else {
			all = (summ + zalog);
			$("#office_"+ officeid +" #countProc").text(ids.length + " шт");
			$("#summZaim").text(zalog + " P");
		}
		$("#office_"+ officeid +" #summProc").text(summ + " Р").parent().attr("data-ids", ids.join(","));
		$("#summAll, #summProcModal").text(all +  " P");
		$("#inpids").val(ids.join(","));
		$("#inpsumm").val(all);
	}
	function startIntrPay() {
		$(".department").each(function(){
			checkSummProc($(this).attr("data-office"));
		});
	}
	$(document).ready(function() {
		startIntrPay();
		$("input[name=\'items[]\']").on("change", function () {
			checkSummProc($(this).attr("data-office"));
		});
		$("#checkall").on("change", function() {
			if ( $(this).is(":checked") ) {
				$("input[name=\'items[]\']").attr("checked", "checked");
			} else {
				$("input[name=\'items[]\']").removeAttr("checked");
			}
			startIntrPay();
		});
		$("#sendpos").on("click", function() {
			var ids = $("#inpids").val();
			var summ = $("#inpsumm").val();
			var dateto = $("#dateto").val();
			$.ajax({
				url: "' . Url::toRoute(['/ajax/sendmail']) . '",
				dataType: "html",
				type: "get",
				data: "ids="+encodeURIComponent(ids)+"&summ="+summ+"&dateto="+dateto,
				success: function(data){
					//$("#sendmailresult, #sendpos").html(data);
					$("#sendpos").html(data);
					$("#sendpos").addClass("disabled").removeAttr("id");
					//console.log(data);
				},
				error: function () {
					$("#sendmailresult").html("Ошибка отправки");
				}
			});
		});
	});
');
	if ($Error != '') {
		echo $Error . '<br />' . $Description;
	} else {
	$count = count($data)-1;
	$part = '';
	$i = $summoffice = $summofficeall = $office = $summZalogCheck = $summZalog = 0;

?>
		<div class="col-lg-9">
			<div class="page-title">Заказать для выкупа на</div>
				<div class="block-select-date">
					<div class="block-select-date__box">
						<input class="js-date-go" type="text" id="dateto" placeholder="<?= $dateto;?>" value="<?= $dateto;?>" />
						<img src="/assets/img/icons/icon-calendar.png" alt="icon-calendar" />
					</div>
				</div>
			<div>
				<div class="checkbox-custome path">
					<label><input type="checkbox" id="checkall" />
					<svg viewBox="0 0 21 21">
						<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
						</svg><span>Выбрать все</span></label>
				</div>
			</div>
<?php
	foreach($data as $number=>$zalog) {
		if ($i == 0 ) { ?>
	<div class="department" id="office_<?=$office;?>" data-office="<?=$office;?>">
		<div class="department__title">Текущие займы по отделению «<?= $zalog->OrgNaim; ?>»</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="department__items">
<?php		}
		$w = 0;
		foreach($zalog->Goods as $item) {
			$w++;
			$ch = false;
			$itrmProc = round($zalog->SumProc / $zalog->SumCredit * $item->SumCredit);
			if (in_array( $number . '-' . $w, $checked)) {
				$ch = true;
				$summoffice += $itrmProc;
				$summZalogCheck += $item->SumCredit;
			}
			$summofficeall += $itrmProc;
			$summZalog += $item->SumCredit;
			
			
?>
					<div class="loan-item-small" data-price="<?= $item->SumCredit;?>">
						<div class="loan-item-small__check">
							<div class="checkbox-custome path"><label><input type="checkbox"<?php if($ch) echo 'checked="checked"';?> name="items[]" value="<?= $itrmProc; ?>" data-office="<?=$office;?>" data-ids="<?= $zalog->NumTicket.'-'.$i; ?>" /><svg viewBox="0 0 21 21">
										<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
									</svg></label></div>
						</div>
						<div class="loan-item-small__media"> <img src="<?= Settings::getPng($item->TypeIzd);?>" alt="<?php echo $item->TypeIzd . ', ' . $item->Proba . ', ' . $item->Ves . ' гр.';?>" /></div>
						<div class="loan-item-small__info"><a class="loan-item-small__number" href="/site/loan-item?id=<?= $number . '-' . $w; ?>">Залоговый<br> билет № <?= $number; ?></a>
							<div class="loan-item-small__desc"><?php echo $item->TypeIzd . ', ' . $item->Proba . '<br />' . $item->Ves . ' гр.';?></div>
						</div>
						<div class="loan-item-small__price"><?= $itrmProc; ?> Р</div>
					</div>
						<?php
		}
		if ($part != $zalog->OrgNaim && $i != $count && $i != 0)  {
			$office++;
										?>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="department__info">
					<p>Общая сумма процентов по отделению «<?= $part; ?>»</p>
					<ul>
						<li><span>Количество, выбранных займов</span><strong id="countProc">1 шт</strong></li>
						<li><span>Итого процентов по займам</span><strong data-val="<?= $summofficeall;?>" id="summProc"><?= $summoffice;?> Р</strong></span>
						<li>
							<div class="checkbox-custome path"><label><input type="checkbox" /><svg viewBox="0 0 21 21">
										<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
									</svg><span>Оплатить баллами</span></label></div><strong class="green">40</strong>
						</li>
					</ul><button class="btn btn-danger btn-block" data-toggle="modal" data-target="#order-redemption">Оплатить проценты</button>
				</div>
			</div>
		</div>
	</div>
	<div class="department" id="office_<?=$office;?>" data-office="<?=$office;?>">
		<div class="department__title">Текущие займы по отделению «<?= $zalog->OrgNaim; ?>»</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="department__items">
<?php		$summoffice=0;
		} else if ($i == $count) {
			if ($summZalogCheck == 0) $allsumm = $summZalog;
			else $allsumm = $summZalogCheck;
		?>

			</div>
		</div>
		<div class="col-sm-4">
			<div class="department__info">
				<p>Общая сумма процентов по отделению «<?= $zalog->OrgNaim; ?>»</p>
				<ul>
					<li><span>Количество, выбранных займов</span><strong id="countProc"><?= count($checked);?> шт</strong></li>
					<li><span>Общая сумма займа</span><strong data-val="<?= $allsumm;?>" id="summZaim"><?= $allsumm; ?> P</strong></li>
					<li><span>Итого процентов по займам</span><strong data-val="<?= $summofficeall;?>" id="summProc"><?= $summoffice;?> Р</strong></li>
					<li><span>Сумма к выкупу</span><strong data-val="<?= ($allsumm + $summofficeall);?>" id="summAll"><?= ($allsumm + $summofficeall); ?> Р</strong></li>
					</ul>
					<div id="sendmailresult"></div>
					<button class="btn btn-secondary btn-block" id="sendpos">Заказать для выкупа</button>
				</div>
			</div>
		</div>
	</div>
<?php	}

		$i++;
		$part = $zalog->OrgNaim;
	} ?>
					</div>
					
<?php } ?>