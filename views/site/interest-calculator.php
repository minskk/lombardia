<?php
use app\models\Settings;

$request = Yii::$app->request;
$ids = $request->get('ids', '');

$this->title = 'Калькулятор расчета процентов';
$this->registerJs('
	$(".js-date-go").datepicker({
		position: "bottom right",
		autoClose: true,
		onSelect: function(formattedDate, date, inst) {
			document.location.href="/site/interest-calculator?date=" + formattedDate + "&ids=' . $ids . '";
		}
	});
	function checkSummProc(officeid) {
		var summ = 0;
		var ids = [];
		$("#office_"+ officeid +" input:checkbox:checked").each(function(){
			summ = summ+parseInt($(this).val());
			ids.push( $(this).attr("data-ids") );
		});
		
		if (summ == 0) {
			summ = parseInt($("#office_"+ officeid +" #summProc").attr("data-val"));
			$("#office_"+ officeid +" #countProc").text("все");
		} else {
			$("#office_"+ officeid +" #countProc").text(ids.length + " шт");
		}
		$("#office_"+ officeid +" #summProc").text(summ + " Р").parent().attr("data-ids", ids.join(","));
		$("#summProcModal").text(summ + " Р");
		$("#inpids").val(ids.join(","));
		$("#inpsumm").val(summ);
	}
	function startIntrPay() {
		$(".department").each(function(){
			checkSummProc($(this).attr("data-office"));
		});
	}
	$(document).ready(function() {
		startIntrPay();
		$("input[name=\'items[]\']").on("change", function () {
			checkSummProc($(this).attr("data-office"));
		});
		$("#checkall").on("change", function() {
			if ( $(this).is(":checked") ) {
				$("input[name=\'items[]\']").attr("checked", "checked");
			} else {
				$("input[name=\'items[]\']").removeAttr("checked");
			}
			startIntrPay();
		});
	});
');
	
if ($Error != '') {
	echo $Error . '<br />' . $Description;
} else {
	$count = count($data)-1;
	$part = '';
	$i = $summoffice = $summofficeall = $office = 0;
	
	if(isset($_GET['date'])) $dt = $_GET['date'];
	else $dt = date('d.m.Y');

?>
		<div class="col-lg-9">
			<div class="page-title">Калькулятор расчета процентов</div>
			<div class="block-select-date">
				<div class="block-select-date__title">Выбрать дату<br> для расчета</div>
				<div class="block-select-date__box"> <span>Дата расчета %</span><input class="js-date-go" type="text" placeholder="<?= $dt;?>" value="<?= $dt;?>"><img src="/assets/img/icons/icon-calendar.png" alt="icon-calendar"></div>
			</div>
			<div>
				<div class="checkbox-custome path">
					<label><input type="checkbox" id="checkall" />
					<svg viewBox="0 0 21 21">
						<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
						</svg><span>Выбрать все</span></label>
				</div>
			</div>
<?php
	foreach($data as $number=>$zalog) {
		if ($i == 0 ) { ?>
						<div class="department" id="office_<?=$office;?>" data-office="<?=$office;?>">
							<div class="department__title">Текущие займы по отделению «<?= $zalog->OrgNaim; ?>»</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="department__items">
<?php		}
		$w = 0;
		foreach($zalog->Goods as $item) {
			$w++;
			$ch = false;
			$itrmProc = round($zalog->SumProc / $zalog->SumCredit * $item->SumCredit);
			if (in_array( $number . '-' . $w, $checked)) {
				$ch = true;
				$summoffice += $itrmProc;
			}
			$summofficeall += $itrmProc;
			
?>
										<div class="loan-item-small">
											<div class="loan-item-small__check">
												<div class="checkbox-custome path"><label><input type="checkbox"<?php if($ch) echo 'checked="checked"';?> name="items[]" value="<?= $itrmProc; ?>" data-office="<?=$office;?>" data-ids="<?= $zalog->NumTicket.'-'.$i; ?>" /><svg viewBox="0 0 21 21">
															<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
														</svg></label></div>
											</div>
											<div class="loan-item-small__media"> <img src="<?= Settings::getPng($item->TypeIzd);?>" alt="<?php echo $item->TypeIzd . ', ' . $item->Proba . ', ' . $item->Ves . ' гр.';?>" /></div>
											<div class="loan-item-small__info"><a class="loan-item-small__number" href="/site/loan-item?id=<?= $number . '-' . $w; ?>">Залоговый<br> билет № <?= $number; ?></a>
												<div class="loan-item-small__desc"><?php echo $item->TypeIzd . ', ' . $item->Proba . '<br />' . $item->Ves . ' гр.';?></div>
											</div>
											<div class="loan-item-small__price"><?= $itrmProc; ?> Р</div>
										</div>
						<?php
		}
		if ($part != $zalog->OrgNaim && $i != $count && $i != 0)  {
			$office++;
										?>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="department__info">
										<p>Общая сумма процентов по отделению «<?= $part; ?>»</p>
										<ul>
											<li><span>Количество, выбранных займов</span><strong id="countProc">1 шт</strong></li>
											<li><span>Итого процентов по займам</span><strong data-val="<?= $summofficeall;?>" id="summProc"><?= $summoffice;?> Р</strong></span>
											<li>
												<div class="checkbox-custome path"><label><input type="checkbox" /><svg viewBox="0 0 21 21">
															<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
														</svg><span>Оплатить баллами</span></label></div><strong class="green">40</strong>
											</li>
										</ul><button class="btn btn-danger btn-block" data-toggle="modal" data-target="#order-redemption">Оплатить проценты</button>
									</div>
								</div>
							</div>
						</div>
						<div class="department" id="office_<?=$office;?>" data-office="<?=$office;?>">
							<div class="department__title">Текущие займы по отделению «<?= $zalog->OrgNaim; ?>»</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="department__items">
<?php		$summoffice=0;
		} else if ($i == $count) {  ?>

									</div>
								</div>
								<div class="col-sm-4">
									<div class="department__info">
										<p>Общая сумма процентов по отделению «<?= $zalog->OrgNaim; ?>»</p>
										<ul>
											<li><span>Количество, выбранных займов</span><strong id="countProc"><?= count($checked);?> шт</strong></li>
											<li><span>Итого процентов по займам</span><strong data-val="<?= $summofficeall;?>" id="summProc"><?= $summoffice;?> Р</strong></li>
											<!--li>
												<div class="checkbox-custome path"><label><input type="checkbox" /><svg viewBox="0 0 21 21">
															<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
														</svg><span>Можно оплатить баллами</span></label></div><strong class="green">40</strong>
											</li-->
										</ul><!--button class="btn btn-danger btn-block" data-toggle="modal" data-target="#order-redemption">Оплатить проценты</button-->
									</div>
								</div>
							</div>
						</div>
<?php	}

		$i++;
		$part = $zalog->OrgNaim;
	} ?>
					</div>
					
<?php } ?>