<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'вход';
// $this->registerJs('
//     $("#send-sms").on("click", function () {
//       var number = $("#login").val();
//             $.ajax({
//                 url: "' . Url::toRoute(['ajax/sms']) . '",
//                 dataType: "html",
// 		        type: "get",
//                 data: "sitelogin="+encodeURIComponent(number),
//                 success: function(data){
//                     $("#entercontent").html(data);
//                     console.log(data);
//                 },
//                 error: function () {
//                     $("#message").html("ERROR");
//                 }
//             });
//         });
//     ');
?>
          <div class="col-lg-6">
            <div class="login-form">
              <div class="login-form__box">
                <div class="login-form__header">
                  <div class="login-form__title">Личный кабинет</div>
                </div>
                <div class="login-form__body">
                  <div class="login-form__links"><a href="#!">Вход</a><!--a href="#!">Стать клиентом </a--></div>
                  <div class="login-form__info">Введите номер телефона в заданном формате, на который придет СМС для авторизации</div>
                  <?= Html::beginForm(['/'], 'post', ['autocomplete' => 'off']); ?>
                    <div id="message">
                        <?php if ($error != '' ) echo ( $error); ?>
                    </div>
                    <div id="entercontent">
                    <?php 
                    if ($try_sess > 0) {
                        echo '<div>отправили СМС с кодом: ' . $pass . '<br />Попыток входа ' . $try_sess . '</div>'; 
                        $inpdata = array('class' => 'form-control form-control-lg', 'id' => 'password', 'autocomplete' => 'off', 'placeholder' => 'Введите код из СМС');
                        $btndata = array('class' => 'btn btn-danger btn-lg btn-block','id' => 'btnsubmit' );
                    ?>
                    
                    <div class="form-group">
                        <label for="password">Пароль из СМС</label>
                        <?= Html::input('password', 'password', '', $inpdata ) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::input('submit', 'enter', 'Войти', $btndata) ?>
                    </div>
                    
                    <?php } else { 
                    ?>
                    <br />
                        (256) 456-46-54<br />
                        (135) 145-46-46<br />
                        (554) 645-64-64<br />
                        (906) 660-26-95<br />
                        (125) 314-65-46<br />
                    <?php
                        $inpdata = array('class' => 'form-control form-control-lg', 'id' => 'login', 'autocomplete' => 'off', 'placeholder' => 'ваш телефон');
                        $btndata = array('class' => 'btn btn-danger btn-lg btn-block','id' => 'btnsubmit');
                    ?>
                    
                    <div class="form-group"><label for="login">Телефон</label>
                    <?= Html::input('text', 'login', '', $inpdata) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::input('submit', 'send-sms', 'Получить код по СМС', $btndata) ?>
                    </div>
                    
                    <?php } ?>
                    </div>
                  <?= Html::endForm() ?>
              </div>
            </div>
          </div>
          <div class="col-lg-3"></div>
