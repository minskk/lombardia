<?php
use app\models\Settings;

// echo '<pre>';var_dump($data);die;

$this->title = 'Оплата процентов';
	if ($Error != '') {
		echo $Error . '<br />' . $Description;
	} else {
	    
        $zalog = $ticket->SumCredit;
        $itrmProc = round($data->SumProc / $data->SumCredit * $zalog);
        $this->registerJs('
        $(document).ready(function() {
            $("#summProcModal").text("' . ($itrmProc + $zalog) . ' P");
        });
        ');
?>
          <div class="col-lg-9">
            <div class="page-title">Информация по займу</div>
            <div class="loan-item-full">
              <div class="loan-item-full-info">
                <div class="loan-item-full-info__state"> <span>Не выкуплено</span></div>
                <div class="loan-item-full-info__box">
                  <div class="loan-item-full-info__media"><img src="<?= Settings::getPng($ticket->TypeIzd, false);?>" alt="<?php echo $ticket->TypeIzd . ', ' . $ticket->Proba . ', ' . $ticket->Ves . ' гр.';?>"></div>
                  <div class="loan-item-full-info__desc">
                    <div class="loan-item-full-info__title"><?php echo $ticket->TypeIzd . ', ' . $ticket->Proba . ', ' . $ticket->Ves . ' гр.';?></div>
                    <div class="loan-item-full-info__number">Залоговый билет № <?= $data->NumTicket; ?></div>
                    <div class="loan-item-full-info__date-start"><strong><?= Settings::normalDate($data->DateZ); ?> </strong><span>Дата выдачи</span></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-8">
                  <div class="loan-item-full-tarif">
                    <ul>
                        <?php
                if (isset($data->RateDescription->NameColumns->Period))
                    foreach ($data->RateDescription->RateTable as $tbl) 
                        
                        echo '<li>' . $data->RateDescription->NameColumns->Period . ': ' . $tbl->Period . ' - ' .
                         $tbl->Percent . $data->RateDescription->NameColumns->Percent . '</li>';
                    else {
                        echo '<pre><small>';echo ($data->RateDescription->RateString);echo '</small></pre>';
                    }
                        ?>
                        
                    </ul>
                  </div>
                  <div class="loan-item-full-date">
                    <ul>
                      <li>Дата выдачи займа: <?= Settings::normalDate($data->DateV);?></li>
                      <li>Дата возврата займа: <?= Settings::normalDate($data->DateR);?></li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="loan-item-full-sum">
                    <ul>
                      <li>Сумма займа: <?= $zalog; ?> Р</li>
                      <li>Начислено%: <?= $itrmProc;?> Р</li>
                      <li>Общая задолженность: <?= $zalog + $itrmProc;?> Р </li>
                    </ul>
                  </div><button class="btn btn-secondary btn-block" data-toggle="modal" data-target="#order-redemption">Заказать для выкупа</button>
                </div>
              </div>
            </div>
          </div>
<?php }