<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Ajax;

$this->title = 'История займов';
$this->registerJs('
    $("#filter-sort").on("change", function () {
        $("#form-sort").submit();
        });
    ');
    if ($Error != '') {
		    echo $Error . '<br />' . $Description;
	} else {
?>
          <div class="col-lg-9">
            <div class="page-title">История займов</div>
            <div class="loan-history-filter"><span>Показывать по</span>
            <?= Html::beginForm(['/site/loan-history'], 'post', array('id' => 'form-sort')); ?>
                <select class="form-control" id="filter-sort" name="sort">
                    <option value="name" value="name"<?= ($filter == 'name')?' selected="selected"':'';?>>Наименованию</option>
                    <option value="date" value="date"<?= ($filter == 'date' || $filter == '')?' selected="selected"':'';?>>По дате</option>
                    <option value="amount" value="amount"<?= ($filter == 'amount')?' selected="selected"':'';?>>По сумме</option>
                </select>
            <?= Html::endForm() ?>
            </div>
            <?php 
            $i = 0;
            foreach($data as $zalog) { 
                    foreach($zalog->Goods as $item) {
                        $i++;
            ?>
            <div class="loan-history-item<?php if($zalog->OplataVozmozhna == 'true') echo ' loan-history-item--not-redeemed';?>">
              <div class="loan-history-item__intro">
                <div class="loan-history-item__info">
                  <div class="loan-history-item__title"><?php echo $item->TypeIzd . ', ' . $item->Proba . ', ' . $item->Ves . "гр.";?></div>
                  <div class="loan-history-item__number">Залоговый билет № <?= $zalog->NumTicket;?></div>
                  <div class="loan-history-item__date-start"> <strong><?= Ajax::normalDate($zalog->DateZ);?></strong><span>Дата залога</span></div>
                </div>
                <div class="loan-history-item__state">
                  <div class="loan-history-item__state-value"><?=($zalog->OplataVozmozhna == 'true')?'не выкуплено':'выкуплено';?></div>
                  <div class="loan-history-item__date-end"> <strong><?= Ajax::normalDate($zalog->DateV);?></strong><span>Дата выдачи</span></div>
                </div>
                <div class="loan-history-item__sum-info">
                  <div class="loan-history-item__assessment"><strong><?= $item->SumCredit;?> Р</strong><span>Оценка</span></div>
                  <div class="loan-history-item__points"><strong>0 Р</strong><span>Баллы<a class="link-info" href="#!"></a></span></div>
                  <div class="loan-history-item__sum"><strong><?= $zalog->SumCredit;?> Р</strong><span>Сумма займа</span></div>
                </div>
              </div>
              <?php if($zalog->OplataVozmozhna == 'true') { ?>
              <div class="loan-history-item__more"> <a class="dropdown-toggle" data-toggle="collapse" href="#collapse<?=$i;?>" role="button" aria-expanded="false" aria-controls="collapse">Подробнее</a>
                <div class="collapse" id="collapse<?=$i;?>">
                    <strong>Тип рассрочки: <?= $zalog->RateDescription->Type;?> </strong><br /><br />
                    <pre><?= $zalog->RateDescription->RateString; ?>
                    <?php
                    
                    // if (isset($zalog->RateDescription->NameColumns->Period))
                    // foreach ($zalog->RateDescription->RateTable as $tbl) 
                    //     echo $zalog->RateDescription->NameColumns->Period . ' - ' . $tbl->Period . ' (' .
                    //      $tbl->Percent . $zalog->RateDescription->NameColumns->Percent . ')<br />';
                    // else var_dump($zalog->RateDescription);
                    ?></pre>
                    </div>
              </div>
              <?php } ?>
            </div>
            <?php }} ?>
          </div>
<?php } ?>