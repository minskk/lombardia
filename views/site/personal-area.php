<?php
$this->title = 'Персональная страница';
?>
          <div class="col-lg-9">
            <div class="personal-area-content">
              <div class="personal-area-item personal-area--user"><a class="personal-area-item__link" href="/user-info.html">
                  <div class="personal-area-item__title">Здравствуйте, Иван Иванов!</div>
                  <div class="personal-area-item__email">ivan@mail.ru</div>
                </a></div>
              <div class="personal-area-item personal-area--payment"><a class="personal-area-item__link" href="/interest-payment.html">
                  <div class="personal-area-item__subtitle">Оплата процентов</div>
                  <div class="personal-area-item__value">341 Р</div>
                </a></div>
              <div class="personal-area-item personal-area--info"><a class="personal-area-item__link" href="#!">
                  <div class="personal-area-item__title">Статус: Участник</div>
                  <div class="personal-area-item__subtitle">Ваши баллы</div>
                  <div class="personal-area-item__value">40</div>
                </a></div>
            </div>
          </div>