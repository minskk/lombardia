<?php
	$this->title = 'Личные данные';
	$session = Yii::$app->session;
	if ($data->Error != '') {
		echo $data->Error . '<br />' . $data->Description;
	} else {
		$fullname = $data->UserInfo->FIO;
		$name = explode(' ', $fullname);
?>
			<div class="col-lg-9">
				<div class="page-title">Личные данные</div>
				<div class="user-info-content">
					<div class="user-info-time">
						<div class="last-visit"> Последний раз Вы заходили <?= $session->get('lastenter'); ?></div>
						<!--div class="data-updates">Последние обновления личных данных вчера в 16:22 </div-->
					</div>
					<div class="user-info-table">
						<div class="table-row">
							<div class="table-cell">ТЕЛЕФОН:</div>
							<div class="table-cell"><?= $session->get('userphone');?></div>
							<div class="table-cell"> <a class="btn-edit" href="#!"></a></div>
							<div class="table-cell">Для входа в личный кабинет</div>
						</div>
						<div class="table-row">
							<div class="table-cell">ИМЯ:</div>
							<div class="table-cell"><?= $name[1];?></div>
							<div class="table-cell"><a class="btn-edit" href="#!"></a></div>
							<div class="table-cell"></div>
						</div>
						<div class="table-row">
							<div class="table-cell">ОТЧЕСТВО:</div>
							<div class="table-cell"><?= $name[2];?>ИЧ</div>
							<div class="table-cell"><a class="btn-edit" href="#!"></a></div>
							<div class="table-cell"></div>
						</div>
						<div class="table-row">
							<div class="table-cell">ФАМИЛИЯ:</div>
							<div class="table-cell"><?= $name[0];?></div>
							<div class="table-cell"><a class="btn-edit" href="#!"></a></div>
							<div class="table-cell"></div>
						</div>
						<!--div class="table-row">
							<div class="table-cell">ДАТА РОЖДЕНИЯ:</div>
							<div class="table-cell">нет информации в ответе АПИ</div>
							<div class="table-cell"><a class="btn-edit" href="#!"></a></div>
							<div class="table-cell"></div>
						</div-->
						<div class="table-row">
							<div class="table-cell">ЭЛЕКТРОННАЯ ПОЧТА:</div>
							<div class="table-cell"><?= ($data->UserInfo->Email != '')?$data->UserInfo->Email:'поле не заполнено';?></div>
							<div class="table-cell"><a class="btn-edit" href="#!"></a></div>
							<div class="table-cell"></div>
						</div>
						<!--div class="table-row">
							<div class="table-cell table-cell-full">
								<div class="checkbox-custome path"><label><input type="checkbox" /><svg viewBox="0 0 21 21">
											<path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
										</svg><span>Хотели ли бы вы получать информацию об интересных акциях и предложениях?</span></label></div>
							</div>
						</div-->
						<!--div class="table-row">
							<div class="table-cell">Смена пароля</div>
							<div class="table-cell"><a href="#!">Cменить пароль</a></div>
							<div class="table-cell"></div>
							<div class="table-cell"></div>
						</div-->
					</div>
				</div>
			</div>
			<?php } ?>