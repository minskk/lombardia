<?php
use app\models\Settings;
$this->title = 'История займов';
$this->registerJs('
    function verifyChecked() {
        
        var summ = 0;
        var ids = [];
        $("#checkchbx input:checkbox:checked").each(function(){
            summ = summ+parseInt($(this).val());
            ids.push( $(this).attr("data-ids") );
        });
        if (ids.length == 0) ids = ["all"];
        
        if (summ == 0) summ = parseInt($("#summProc").attr("data-val"));
        $("#summProc").text(summ + " Р").parent().attr("data-ids", ids.join(","));
    }
    $("input[name=\'items[]\']").on("change", function () {
        verifyChecked();
    });
    $(document).ready(function() {
        verifyChecked();
    });
    ');
    
    if ($Error != '') {
		    echo $Error . '<br />' . $Description;
	} else {
?>
          <div class="col-lg-9">
            <div class="page-title">Мои займы</div>
            <pre><?php// var_dump($data); ?></pre>
            <div class="my-loans-content">
              <div class="personal-area-item personal-area--payment"><a id="ids" class="personal-area-item__link" href="#" onclick="document.location.href='/site/interest-payment?ids=' + $(this).attr('data-ids'); return false;">
                  <div class="personal-area-item__subtitle">Оплата процентов</div>
                  <div class="personal-area-item__value" id="summProc" data-val="<?= $summProc;?>"><?= $summProc;?> Р</div>
                  <div class="personal-area-item__subtitle">Сумма процентов на сегодня</div>
                </a></div>
              <div class="block-links">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="block-link"> <a class="block-link__link" href="/site/interest-calculator" onclick="document.location.href='/site/interest-calculator?ids=' + $('#ids').attr('data-ids'); return false;"><span class="block-link__title">Калькулятор<br> расчета процентов</span><img class="block-link__icon" src="/assets/img/icons/icon-calc.svg" alt="Калькулятор расчета процентов"></a></div>
                  </div>
                  <div class="col-sm-6">
                    <div class="block-link"> <a class="block-link__link" href="/site/order-redemption" onclick="document.location.href='/site/order-redemption?ids=' + $('#ids').attr('data-ids'); return false;"><span class="block-link__title">Заказать<br> для выкупа</span><img class="block-link__icon" src="/assets/img/icons/icon-cart.svg" alt="Заказать для выкупа"></a></div>
                  </div>
                </div>
              </div>
              <div class="my-loans-items mt-5" id="checkchbx">
                  <?php 
                  $OrgNaim = '';
                  foreach($data as $zalog) { 
                  $i = 0;
                  
                  if($OrgNaim != $zalog->OrgNaim) {
                      $OrgNaim = $zalog->OrgNaim;
                  ?>
                <div class="my-loans-items__title"> Текущие займы по филиалу «<?= $OrgNaim; ?>»</div>
                    <?php 
                  }
                    foreach($zalog->Goods as $item) { 
                        $i++;
                        $itrmProc = round($zalog->SumProc / $zalog->SumCredit * $item->SumCredit);
                    ?>
                <div class="loan-item">
                  <div class="loan-item__check">
                    <div class="checkbox-custome path"><label><input type="checkbox" name="items[]" value="<?= $itrmProc; ?>" data-ids="<?= $zalog->NumTicket.'-'.$i; ?>" /><svg viewBox="0 0 21 21">
                          <path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
                        </svg></label></div>
                  </div>
                  <div class="loan-item__media"> <img src="<?= Settings::getPng($item->TypeIzd);?>" alt="<?php echo $item->TypeIzd . ', ' . $item->Proba . ', ' . $item->Ves . ' гр.';?>" /></div>
                  <div class="loan-item__info"><a class="loan-item__number" href="/site/loan-item?id=<?= $zalog->NumTicket.'-'.$i; ?>">Залоговый<br> билет № <?= $zalog->NumTicket; ?></a>
                    <div class="loan-item__desc"><?php echo $item->TypeIzd . ', ' . $item->Proba . '<br />' . $item->Ves . ' гр.';?></div>
                  </div>
                  <div class="loan-item__btns"> 
                      <a class="loan-item__btn loan-item__pay-interest" href="/site/interest-payment?ids=<?= $zalog->NumTicket.'-'.$i; ?>"><span>Оплатить проценты</span><br/><span><?= $itrmProc; ?> Р</span></a>
                      <a class="loan-item__btn loan-item__order-removal" href="/site/order-redemption?ids=<?= $zalog->NumTicket.'-'.$i; ?>"><span>Заказать<br> для выкупа</span></a>
                      <a class="loan-item__btn loan-item__remedy-calculator" href="/site/interest-calculator?ids=<?= $zalog->NumTicket.'-'.$i; ?>"><span>Калькулятор выкупа<br> на </span><span> <?= date('d.m.y'); ?></span></a></div>
                </div>
                <?php }} ?>
              </div>
            </div>
          </div>
<?php } ?>