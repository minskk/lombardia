<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;


class Ajax extends Model {
	
	public static function sendMess($ids, $summ, $dateto) {
		$session = Yii::$app->session;
		$phone = trim($session->get('userphone'));
		
		$text = <<<QWE
		<p>текст сообщения</p>
		<p>$ids</p>
		<p>$summ</p>
		<p>$dateto</p>
		<p>$phone</p>
QWE;
		
		Yii::$app->mailer->compose()
			->setFrom('admin@4455.by')
			->setTo('a.s.minsk@mail.ru')
			->setSubject('Тема сообщения')
			// ->setTextBody($text)
			->setHtmlBody($text)
			->send();

		return 'Заказ отправлен';
	}
	
	public static function getUser() {
		$session = Yii::$app->session;
		return Yii::$app->db->createCommand("SELECT * FROM y_users WHERE phone='" . $session->get('userphone') . "'")->queryOne();
	}
	
	public static function PostSendSMS($userphone) {
		$session = Yii::$app->session;
		
		$pass = rand(10000, 99999);
		$session->set('smscode', $pass);
		$session->set('try', 5);
		$session->set('userphone', $userphone);
		
		$html = '';
		$html .= 'Код из СМС: ' . $pass;
		
		$inp = Html::input('password', 'password', '', $inpdata );
		$btn = Html::input('submit', 'enter', 'Войти', $btndata);
		
		$html .= <<<EOT
					  <div class="form-group">
						<label for="password">Пароль из СМС</label>
						$inp
					</div>
					<div class="form-group">
						$btn
					</div>
EOT;
		$function = 'PostSendSMS';
		return $html;
	}
	
	public static function PostUserInfo() {
		$function = 'PostUserInfo';
		$data = array(
			'Tel' => self::userphone(),
			"Date" => self::getDate()
			);
		return array('data' => self::callAPI($data, $function));
	}
	
	public static function PostUserInfoVerify($tel) {
		$function = 'PostUserInfo';
		$data = array(
			'Tel' => $tel,
			"Date" => self::getDate()
			);
		return self::callAPI($data, $function);
	}
	
	public static function PostInfoZalogodatel($filter = false) {
		$function = 'PostInfoZalogodatel';
		$data = array(
			'Tel' => self::userphone(),
			"Date" => self::getDate()
			);
		
		$resp = self::callAPI($data, $function);
		
		$ret = array();
		switch ($filter) {
			case 'name':
				foreach ($resp->TicketList as $key=>$item) $ret[$item->OrgNaim.$key] = $item;
			break;
			case 'amount':
				foreach ($resp->TicketList as $key=>$item) $ret[$item->SumCredit.$key] = $item;
			break;
			default:
				foreach ($resp->TicketList as $key=>$item) {
					$datez = str_replace(array('-', 'T', ':'), '', $item->DateZ);
					$ret[$datez.$key] = $item;
				}
			break;
		}
		$summProc = 0;
		foreach ($ret as $zal) $summProc += $zal->SumProc;
		
		if ($filter == 'amount') krsort($ret);
		else ksort($ret);
		
		return array(
			'filter' => $filter,
			'data' => $ret,
			'Error' => $resp->Error,
			'Description' => $resp->Description,
			'summProc' => $summProc
		);
	}
	
	public static function PostInfoZalogodatelPayment($ids, $date = '') {
		$function = 'PostInfoZalogodatel';
		
		if ($date == '' ) {
			$date = date("d.m.Y");
			$to = self::getDate();
		} else {
			$part = explode('.', $date);
			$to = $part[2] . '-' . $part[1] . '-' . $part[0] . 'T' . date('h:i:s');
		}
		
		$data = array(
			'Tel' => self::userphone(),
			"Date" => $to
			);
			
		if ($ids) $idsarr = explode(',', $ids);
		else $idsarr = array();
		
		$resp = self::callAPI($data, $function);
		$ret = array();
		
		if ( !isset( $resp->TicketList ) ) return array( 'data' => $resp );
		
		foreach ($resp->TicketList as $key=>$item) $ret[$item->NumTicket] = $item;
		return array(
			'checked' => $idsarr,
			'data' => $ret,
			'dateto' => $date,
			'Error' => $resp->Error,
			'Description' => $resp->Description
		);
	}

	public static function PostInfoItem($id) {
		$function = 'PostInfoZalogodatel';

		$to = self::getDate();
		$data = array(
			'Tel' => self::userphone(),
			"Date" => $to
			);
		$resp = self::callAPI($data, $function);
		$part = explode('-', $id);
		
		foreach($resp->TicketList as $it) {
			if ($it->NumTicket == $part[0]) {
				$allTicket = $it;
				$ticket = $it->Goods[intval($part[1]) - 1];
			}
		}
		
		return array(
			'data' => $allTicket,
			'ticket' => $ticket,
			'Error' => $resp->Error,
			'Description' => $resp->Description
		);
	}
	
	private static function callAPI($data, $function) {
		$url = 'http://85.30.228.34:7780/Test/hs/LK_Zalogodatel/'.$function;
		$login = 'lk1';
		$password = '789456';
		$header = array(
				'Content-Type: application/json'
			);
		$postfields = json_encode($data);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return json_decode($response);
	}
	
	private function getDate() {
		return date('Y-m-d') . 'T' . date('h:i:s');
	}
	
	private function userphone() {
		$session = Yii::$app->session;
		return trim($session->get('userphone'));
	}
	
	public static function normalDate($date) {
		$t = explode('T', $date);
		$s = explode('-', $t[0]);
		return $s[2] . '.' . $s[1] . '.' . $s[0];
	}
}