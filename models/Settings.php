<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class Settings extends Model {
	
	public static function checkSession() {
		$session = Yii::$app->session;
		if ($session->has('registeruser') && $session->get('registeruser') == 'y') return false;
		return true;
	}
	
	public static function getPng($word, $big=false) {
		$data = array(
			'1' => 'Блюд',
			'2' => 'Бокал',
			'3' => 'Брасл',
			'4' => 'Брел',
			'5' => 'Брош',
			'6' => 'Булав',
			'7' => 'Ваз',
			'8' => 'Вилк',
			'6'	=> 'Заж',
			'9' => 'Заколк',
			'10' => 'Запон',
			'11' => 'брилл',
			'12' => 'Колье',
			'13' => 'Кольцо с',
			'14' => 'Кольцо',
			'15' => ' час',
			'16' => 'Крест',
			'17' => 'Кулон с ц',
			'18' => 'Кулон',
			'19' => 'Ложк',
			'20' => 'Медаль',
			'21' => 'Нож',
			'22' => 'Ожер',
			'23' => 'сер',
			'24' => 'Пирс',
			'25' => 'Подвес',
			'26' => 'Подн',
			'27' => 'Порт',
			'28' => 'Пудр',
			'29' => 'Рюм',
			'30' => 'Серь',
			'31' => 'Сите',
			'32' => 'Стак',
			'33' => 'Стоп',
			'34' => 'Табак',
			'35' => 'Тарел',
			'36' => 'Цеп',
			'37' => 'Час'
			);
			
			if($big) $suff = '-big';
			else $suff = '';
			
			$rnd = rand (1, 37);
			$ret = '/assets/img/loan-item-' . $rnd . $suff . '.png';
			foreach($data as $key => $lett) 
				if (stristr($word, $lett)) 
					$ret = '/assets/img/loan-item-' . $key . $suff . '.png';
			
			return $ret;
	}
	
	public static function normalDate($date) {
		$t = explode('T', $date);
		$s = explode('-', $t[0]);
		return $s[2] . '.' . $s[1] . '.' . $s[0];
	}
}