<?php
namespace app\widgets;

use Yii;
use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class Menulomb extends Menu {
	public $items = array(
					['label' => 'О нас',				'aclass' => 'nav-link', 'url' => ['site/about']],
					['label' => 'Наши услуги', 			'aclass' => 'nav-link', 'url' => ['site/services']],
					['label' => 'Прейскурант',			'aclass' => 'nav-link', 'url' => ['site/services']],
					['label' => 'Тарифы',				'aclass' => 'nav-link', 'url' => ['site/contact']],
					['label' => 'Акции',				'aclass' => 'nav-link', 'url' => ['site/contact']],
					['label' => 'Юлелирный дисконт',	'aclass' => 'nav-link', 'url' => ['site/contact']],
					['label' => 'Адреса ломбардов',		'aclass' => 'nav-link', 'url' => ['site/contact']],
				);
	public $itemOptions = array( 'class' => 'nav-item' );
	public $options = array( 'class' => 'nav' );
	
	public $linkTemplate = '<a class="{aclass}" href="{url}">{label}</a>';
	
	protected function renderItem($item) {
		if (isset($item['url'])) {
			$template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

			return strtr($template, [
				'{url}' => Html::encode(Url::to($item['url'])),
				'{label}' => $item['label'],
				'{aclass}' => $item['aclass'],
			]);
		}

		$template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

		return strtr($template, [
			'{label}' => $item['label'],
		]);
	}
}