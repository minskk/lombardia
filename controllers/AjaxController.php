<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Ajax;

class AjaxController extends Controller
{
	public function actionSendmail() {
		if ($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$result = Ajax::sendMess($request->get('ids'), $request->get('summ'), $request->get('dateto'));
		
		echo $result;
		die;
	}
	
	public function actionSms() {
		if ($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		
		$phone = $request->get('sitelogin', false);
		echo Ajax::PostSendSMS(urldecode($phone));
		
		die;
	}
	
	public function actionPaykeeper() {
		if ($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$session = Yii::$app->session;
		
		$orderid = $request->get('inpids', '');
		$order_sum = $request->get('inpsumm', 0);
		if($orderid == '') $orderid = 'все текущие';
		
		$data = Ajax::PostUserInfo();
		$client_login = $data['data']->UserInfo->FIO;
		
		$phone = trim($session->get('userphone'));
		
// 		var_dump($client_login,$orderid,$order_sum,$phone); die;
		
		$payment_parameters = http_build_query(array(
			"clientid" => $client_login,
			"orderid" => $orderid,
			"sum" => $order_sum,
			"client_phone" => $phone
			));
		$options = array(
			"http"=>array(
			"method"=>"POST",
			"header"=>
			"Content-type: application/x-www-form-urlencoded",
			"content"=>$payment_parameters
		   ));
		$context = stream_context_create($options);
		
		echo file_get_contents("https://demo.paykeeper.ru/order/inline/",FALSE, $context);
		die;
	}
	
	private function checkNoSession() {
		$session = Yii::$app->session;
		if ($session->has('registeruser') && $session->get('registeruser') == 'y') return false;
		return true;
	}
}