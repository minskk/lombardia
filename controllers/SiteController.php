<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ajax;

class SiteController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$session = Yii::$app->session;
		$sitelogin = $request->post('login', '');
		$password = $request->post('password', '');
		$try_sess = $pass = 0;
		$vf = $error = $pass = '';
		
		$now = date('d-m-Y H:i:s');
		 
		if ($sitelogin != '') {
			$vf = Ajax::PostUserInfoVerify( $sitelogin );
			if( $vf->Error != '' ) $error =  $vf->Error . '<br />' . $vf->Description;
			else {
				$try_sess = 5;
				$pass = rand(10000, 99999);
				$session->destroy();
				$session->open();
				$session->set('userphone', $sitelogin);
				$session->set('smscode', $pass);
				$session->set('try', $try_sess);

				$user = Ajax::getUser();
				if($user === false) {
					$ex = Yii::$app->db->createCommand()->insert('y_users', [
						'phone' => $session->get('userphone'),
						'enter' => $try_sess,
						'ip' => Yii::$app->request->userIP,
						'data' => $now,
						'blocked' => 0,
					])->execute();
					$session->set('lastenter', $now);
				} else {
					$session->set('lastenter', $user['data']);
					Yii::$app->db->createCommand("UPDATE y_users SET blocked=0, data='" . $now . "', enter=" . $try_sess . ", ip='" . Yii::$app->request->userIP . "' WHERE id=" . $user['id'])->execute();
				}
				// echo '<pre>';var_dump($user);echo '</pre>';
			}
			
		} else if ($password != '' && $session->has('smscode') && $session->get('smscode') == $password) {
			if (!$session->isActive) $session->open();
			$session->set('registeruser', 'y');
			
		} else if ($password != '' && $session->has('try') && $session->get('try') > 0) {
			$try_sess = $session->get('try');
			$try_sess--;
			$session->set('try', $try_sess );
			$pass = $session->get('smscode');
			$user = Ajax::getUser();
			Yii::$app->db->createCommand("UPDATE y_users SET data='" . $now . "', blocked=0, enter=" . $try_sess . ", ip='" . Yii::$app->request->userIP . "' WHERE id=" . $user['id'])->execute();
		}
		
		if( !$this->checkNoSession() ) {
			$user = Ajax::getUser();
			Yii::$app->db->createCommand("UPDATE y_users SET data='" . $now . "', blocked=1, ip='" . Yii::$app->request->userIP . "' WHERE id=" . $user['id'])->execute();
			return $this->redirect('/site/my-loans');
		} else
			return $this->render('index', array('try_sess' => $try_sess, 'pass' => $pass, 'error' => $error));
	}
	
	public function actionExit() {
		$session = Yii::$app->session;
		$session->destroy();
		$this->redirect('/');
	}
	
	public function actionUserInfo() {
		if ($this->checkNoSession()) return $this->redirect('/');
		$data = Ajax::PostUserInfo();
		return $this->render('user-info', $data);
	}
	
	public function actionLoanHistory() {
		if ($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$filter = $request->post('sort', false);
		$data = Ajax::PostInfoZalogodatel($filter);
		return $this->render('loan-history', $data);
	}
	
	public function actionMyLoans() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$data = Ajax::PostInfoZalogodatel(false);
		return $this->render('my-loans', $data);
	}
	
	public function actionInterestPayment() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$ids = $request->get('ids', false);
		$data = Ajax::PostInfoZalogodatelPayment($ids);
		return $this->render('interest-payment', $data);
	}
	
	public function actionOrderRedemption() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$ids = $request->get('ids', false);
		$date = $request->get('date', '');
		$data = Ajax::PostInfoZalogodatelPayment($ids, $date);
		return $this->render('order-redemption', $data);
	}
	
	public function actionInterestCalculator() {
		if($this->checkNoSession()) return $this->redirect('/');
		$session = Yii::$app->session;
		
		// echo '<pre>';var_dump($ex, $posts);echo '</pre>';
		
		$request = Yii::$app->request;
		$ids = $request->get('ids', false);
		$date = $request->get('date', '');
		$data = Ajax::PostInfoZalogodatelPayment($ids, $date);
		return $this->render('interest-calculator', $data);
	}
	
	public function actionLoanItem() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$request = Yii::$app->request;
		$id = $request->get('id', false);
		$data = Ajax::PostInfoItem($id);
		return $this->render('loan-item', $data);
	}
	
	public function actionPersonalArea() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$data = Ajax::PostInfoZalogodatel(false);
		return $this->render('personal-area', $data);
	}
	
	public function actionPromotions() {
		if($this->checkNoSession()) return $this->redirect('/');
		
		$data = Ajax::PostUserInfo();
		return $this->render('promotions', $data);
	}
	/**
	 * Login action.
	 *
	 * @return Response|string
	 */
	public function actionLogin()
	{
		if($this->checkNoSession()) return $this->redirect('/');
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout()
	{
		if($this->checkNoSession()) return $this->redirect('/');
		
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * Displays contact page.
	 *
	 * @return Response|string
	 */
	public function actionContact()
	{
		if($this->checkNoSession()) return $this->redirect('/');
		
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}
		return $this->render('contact', [
			'model' => $model,
		]);
	}

	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout()
	{
		if($this->checkNoSession()) return $this->redirect('/');
		
		$url = 'http://85.30.228.34:7780/Test/hs/LK_Zalogodatel/PostUserInfo';
		$login = 'lk1';
		$password = '789456';
		$header = array(
				'Content-Type: application/json'
			);
		$payloadName = array(
			'Tel' => '+7 (451) 315-44-56',
			"Date" => "2021-02-05T11:59:51"
			);
		$postfields = json_encode($payloadName);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);
		curl_close($ch);
		$data['req'] = json_decode($response);
		
		return $this->render('about', $data);
	}
	
	private function checkNoSession() {
		$session = Yii::$app->session;
		if ($session->has('registeruser') && $session->get('registeruser') == 'y') return false;
		return true;
	}
}
